---
### Deployment
Each environment has a separate backend state and configuration file, which are stored in the vars/ directory. Below is an example of deploying the dev environment:
> Initialisation
```bash
terraform init --backend-config vars/devel-backend.tfvars
```

```bash
terraform  apply --var-file vars/devel.tfvars 
```
---
### Postinstall
After a successful deployment of the application, we need to set up a VPN connection.
- The first step is to download the AWS Client VPN.
- Then, we need to add the profile we created during the SSL Certificate creation. Open the "AWS VPN Client," click on "Manage profiles," then select "Add profile" and click on "Connect." 
- You should now be connected to our Client VPN Endpoint.
- Please note that each environment requires a separate VPN connection.