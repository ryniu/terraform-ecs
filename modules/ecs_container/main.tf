#
locals {
  lb_port     = var.health_check_port_app
  lb_name     = "${var.environment_name}-${var.name}-ext-${var.protocol == "tcp" ? "lb" : "alb"}"
  tg_name     = "${var.environment_name}-${var.name}-lb-tg${var.ssl ? "-ssl" : ""}"
  sg_list     = [var.security_group]
  certificate = var.ssl ? var.wild_cert_arn : null
  ssl_policy  = var.ssl ? "ELBSecurityPolicy-2016-08" : null
}

resource "aws_ecs_task_definition" "task" {
  family                   = var.name
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.task_cpu
  memory                   = var.task_mem
  execution_role_arn       = var.execution_role_arn
  task_role_arn            = var.execution_role_arn
  container_definitions = jsonencode([
    {
      name  = var.name
      image = var.container_image
      cpu       = var.container_cpu
      memory    = var.container_mem
      essential = true
      logConfiguration = {
        logDriver = "awslogs"
        options = {
          awslogs-group         = "/${var.project_name}/${var.environment_name}/ecs/${var.name}",
          awslogs-region        = var.aws_region,
          awslogs-stream-prefix = "${var.name}-task",
        }
      }
      entryPoint   = var.entrypoint_data
      environment  = var.environment
      portMappings = var.ports
      secrets      = var.secrets
    }
  ])
}

resource "aws_ecs_service" "service" {
  name                   = var.name
  cluster                = var.cluster_id
  task_definition        = aws_ecs_task_definition.task.arn
  enable_execute_command = var.enable_execute_command
  desired_count          = var.container_count
  launch_type            = "FARGATE"
  platform_version       = "1.4.0"

  load_balancer {
    target_group_arn = aws_lb_target_group.tg.arn
    container_name   = aws_ecs_task_definition.task.family
    container_port   = local.lb_port
  }
  network_configuration {
    assign_public_ip = true
    subnets          = flatten([var.subnets])
    security_groups  = [var.security_group]
  }
  depends_on = [
    aws_ecs_task_definition.task,
    aws_lb_listener.listener,
    aws_lb_target_group.tg,
  ]
}

resource "aws_lb" "lb" {
  name                 = local.lb_name
  internal             = true
  preserve_host_header = true
  load_balancer_type   = "application"
  subnets              = var.subnets
  security_groups      = local.sg_list
  tags = {
    Name = "${var.name}-${var.environment_name}-ecs-lb"
  }
}

resource "aws_lb_listener" "listener" {
  load_balancer_arn = aws_lb.lb.arn
  port              = "80"
  protocol          = "HTTP"
  ssl_policy        = local.ssl_policy
  certificate_arn   = local.certificate
  default_action {
    target_group_arn = aws_lb_target_group.tg.arn
    type             = "forward"
  }
}


resource "aws_lb_target_group" "tg" {
  name        = "${var.environment_name}-${var.name}-lb-tg"
  port        = var.health_check_port_app
  protocol    = "HTTP"
  vpc_id      = var.vpc_id
  target_type = "ip"
  health_check {
    interval            = 130
    timeout             = 120
    healthy_threshold   = 10
    unhealthy_threshold = 10
    protocol            = "HTTP"
    port                = var.health_check_port_app
    matcher             = "200,302,401"
  }
}


resource "aws_route53_record" "local_lbrecord" {
  zone_id = var.zone_id
  name    = "${var.name}.${var.dns_name_local}"
  type    = "CNAME"
  ttl     = "300"
  records = [aws_lb.lb.dns_name]
}

output "lb_listener" {
  value = aws_lb_listener.listener.arn
}