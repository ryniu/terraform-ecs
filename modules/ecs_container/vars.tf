variable "app_ssl_port" {
  type    = number
  default = 1
}

variable "https_port" {
  type    = number
  default = null
}

variable "aws_region" {
  type    = string
  default = ""
}

variable "security_group" {
  type    = string
  default = ""
}

variable "cluster_id" {
  type    = string
  default = ""
}

variable "container_image" {
  type    = string
  default = ""
}

variable "container_cpu" {
  type    = number
  default = null
}

variable "container_mem" {
  type    = number
  default = null
}

variable "dns_name_local" {
  type    = string
  default = ""
}

variable "environment" {
  type = list(object({
    name  = string
    value = string
  }))
  default = []
}

variable "environment_name" {
  type    = string
  default = ""
}

variable "execution_role_arn" {
  type    = string
  default = ""
}

variable "scale_role_arn" {
  type    = string
  default = ""
}

variable "container_count" {
  type    = number
  default = 1
}

variable "enable_execute_command" {
  type    = bool
  default = "false"
}

variable "entrypoint_data" {
  type    = list(string)
  default = []
}

variable "health_check_path" {
  type    = any
  default = null
}

variable "health_check_interval" {
  type    = number
  default = null
}

variable "health_check_healthy_threshold" {
  type    = number
  default = null
}

variable "health_check_unhealthy_threshold" {
  type    = number
  default = null
}

variable "health_check_port_app" {
  type    = number
  default = null
}

variable "name" {
  type    = string
  default = ""
}

variable "non_ssl" {
  type    = bool
  default = false
}

variable "auto_scale" {
  type    = number
  default = "0"
}

variable "subnets_p" {
  type    = any
  default = ""
}

variable "subnets" {
  type    = any
  default = ""
}

variable "protocol" {
  type    = string
  default = "HTTP"
}

variable "ssl" {
  type    = bool
  default = false
}

variable "task_cpu" {
  type    = number
  default = null
}

variable "task_mem" {
  type    = number
  default = null
}

variable "ports" {
  type = list(object({
    hostPort      = number
    containerPort = number
  }))
  default = []
}

variable "secrets" {
  type = list(object({
    name      = string
    valueFrom = string
  }))
  default = []
}

variable "wild_cert_arn" {
  type    = string
  default = ""
}

variable "vpc_id" {
  type    = string
  default = ""
}

variable "zone_id" {
  type    = string
  default = ""
}


variable "lb_count" {
  type    = number
  default = "0"
}

variable "listener_count" {
  type    = number
  default = "0"
}
variable "tg_count" {
  type    = number
  default = "0"
}
variable "route53_record" {
  type    = string
  default = "dev"
}
variable "project_name" {
  type    = string
  default = ""
}
