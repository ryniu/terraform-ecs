variable "db_name" {
  type    = string
  default = ""
}

variable "db_user" {
  type    = string
  default = ""
}

variable "db_pass" {
  type    = string
  default = ""
}

variable "db_size" {
  type    = number
  default = null
}

variable "db_machine" {
  type    = string
  default = ""
}

variable "db_version" {
  type    = number
  default = null
}

variable "database_sg_id" {
  type    = string
  default = ""
}

variable "dns_name_local" {
  type    = string
  default = ""
}

variable "environment_name" {
  type    = string
  default = ""
}

variable "subnets" {
  type    = list(string)
  default = []
}

variable "zone_id" {
  type    = string
  default = ""
}