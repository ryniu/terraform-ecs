resource "aws_db_subnet_group" "default" {
  name       = "${var.db_name}-${var.environment_name}"
  subnet_ids = var.subnets
  tags = {
    Name = "${var.db_name}-${var.environment_name}-rds-subnet-group"
  }
}
resource "aws_db_parameter_group" "default" {
  name   = replace(lower("${var.db_name}-${var.environment_name}"), "_", "-")
  family = "postgres15"
  parameter {
    name  = "log_connections"
    value = "1"
  }
  parameter {
    name  = "log_disconnections"
    value = "1"
  }
  lifecycle {
    create_before_destroy = true
  }
  tags = {
    Name = "${var.db_name}-${var.environment_name}-rds-parameter-group"
  }
}

resource "aws_db_instance" "database" {
  identifier                 = replace(lower("${var.db_name}-${var.environment_name}"), "_", "-")
  instance_class             = var.db_machine
  allocated_storage          = var.db_size
  engine                     = "postgres"
  engine_version             = var.db_version
  username                   = var.db_user
  password                   = var.db_pass
  db_name                    = var.db_name
  db_subnet_group_name       = aws_db_subnet_group.default.name
  parameter_group_name       = aws_db_parameter_group.default.name
  vpc_security_group_ids     = [var.database_sg_id]
  auto_minor_version_upgrade = false
  publicly_accessible        = false
  skip_final_snapshot        = true
  tags = {
    Name = "${var.db_name}-${var.environment_name}-rds-instance"
  }
  depends_on = [
    aws_db_subnet_group.default,
    aws_db_parameter_group.default,
    aws_db_subnet_group.default
  ]
}

resource "aws_route53_record" "local_dbrecord" {
  zone_id = var.zone_id
  name    = "${var.db_name}.${var.dns_name_local}"
  type    = "CNAME"
  ttl     = "300"
  records = [aws_db_instance.database.address]
}

output "db_name" {
  value = var.db_name
}

output "db_user" {
  value = var.db_user
}

output "db_pass" {
  value = var.db_pass
}

output "db_dns_name" {
  value = aws_route53_record.local_dbrecord.name
}
