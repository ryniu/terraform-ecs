# Public zone is set manually, because of the NS change reason
data "aws_route53_zone" "public" {
  name = var.dns_name
}

output "public_zone_id" {
  value = data.aws_route53_zone.public.zone_id
}
