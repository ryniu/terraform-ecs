resource "aws_ecs_cluster" "main" {
  name = "${var.project_name}-${var.environment_name}"
  tags = {
    Name = "${var.project_name}-${var.environment_name}-ecs-cluster"
  }
  lifecycle {
    create_before_destroy = true
    ignore_changes        = [name]
  }
}

resource "aws_cloudwatch_log_group" "log_group" {
  name              = "/ecs/${var.project_name}-${var.environment_name}"
  retention_in_days = "90"
  tags = {
    Name = "${var.project_name}-${var.environment_name}-ecs-loggroup"
  }
  lifecycle {
    create_before_destroy = true
    ignore_changes        = [name]
  }
}

output "cluster_id" {
  value = aws_ecs_cluster.main.id
}

output "cluster_arn" {
  value = aws_ecs_cluster.main.arn
}