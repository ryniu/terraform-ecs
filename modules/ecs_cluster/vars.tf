variable "environment_name" {
  type    = string
  default = ""
}

variable "project_name" {
  type    = string
  default = ""
}
