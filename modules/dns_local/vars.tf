variable "dns_name_local" {
  type    = string
  default = ""
}

variable "dns_name" {
  type    = string
  default = ""
}
variable "vpc_id" {
  type    = string
  default = ""
}