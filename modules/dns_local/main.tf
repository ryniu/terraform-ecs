## Private zone will be used for local database communication
resource "aws_route53_zone" "private" {
  name = var.dns_name_local
  vpc {
    vpc_id = var.vpc_id
  }
}

output "private_zone_id" {
  value = aws_route53_zone.private.zone_id
}
