resource "aws_cloudwatch_log_group" "log_group" {
  for_each = var.resources

  name              = "/${var.project_name}/${var.environment_name}/${each.value.resource_type}/${each.key}"
  retention_in_days = each.value.retention_in_days

  tags = {
    Name = "${var.project_name}-${var.environment_name}-${each.value.resource_type}-${each.key}-logs"
  }
}

output "log_group_arns" {
  value = { for k, v in aws_cloudwatch_log_group.log_group : k => v.arn }
}

output "log_group_name" {
  value = { for k, v in aws_cloudwatch_log_group.log_group : k => v.name }
}