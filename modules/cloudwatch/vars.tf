variable "project_name" {
  type = string
}

variable "environment_name" {
  type = string
}

variable "resources" {
  type = map(object({
    resource_type     = string
    retention_in_days = number
  }))
}

