variable "subnets" {
  type    = any
  default = ""
}

variable "security_group" {
  type    = any
  default = ""
}

variable "rest_api_listener" {
  type    = string
  default = ""
}