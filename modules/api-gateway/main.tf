resource "aws_apigatewayv2_vpc_link" "vpclink_apigw_to_alb" {
  name        = "vpclink_apigw_to_alb"
  security_group_ids = []
  subnet_ids = var.subnets
}

resource "aws_apigatewayv2_api" "apigw_http_endpoint" {
  name          = "rest-api-pvt-endpoint"
  protocol_type = "HTTP"
}

resource "aws_apigatewayv2_integration" "rest_api_integration" {
  api_id           = aws_apigatewayv2_api.apigw_http_endpoint.id
  integration_type = "HTTP_PROXY"
  integration_uri  = var.rest_api_listener

  integration_method = "ANY"
  connection_type    = "VPC_LINK"
  connection_id      = aws_apigatewayv2_vpc_link.vpclink_apigw_to_alb.id
  payload_format_version = "1.0"
  request_parameters = {
    "overwrite:path"  = "/$request.path.proxy"
  }
  depends_on      = [aws_apigatewayv2_vpc_link.vpclink_apigw_to_alb,
    aws_apigatewayv2_api.apigw_http_endpoint]
}

resource "aws_apigatewayv2_route" "rest_api_route" {
  api_id    = aws_apigatewayv2_api.apigw_http_endpoint.id
  route_key = "ANY /test/{proxy+}"
  target = "integrations/${aws_apigatewayv2_integration.rest_api_integration.id}"
  depends_on  = [aws_apigatewayv2_integration.rest_api_integration]
}

resource "aws_apigatewayv2_stage" "apigw_stage" {
  api_id = aws_apigatewayv2_api.apigw_http_endpoint.id
  name   = "$default"
  auto_deploy = true
  depends_on  = [aws_apigatewayv2_api.apigw_http_endpoint]
}

output "apigw_endpoint" {
  value = aws_apigatewayv2_api.apigw_http_endpoint.api_endpoint
  description = "API Gateway Endpoint"
}
