# Get avaliability zones
data "aws_availability_zones" "available_zones" {
  state = "available"
}

# Setup VPC
resource "aws_vpc" "default" {
  cidr_block           = var.vpc_cidr
  enable_dns_hostnames = true
  enable_dns_support   = true
  tags = {
    Name = "${var.project_name}-${var.environment_name}-vpc"
  }
}

# Internet gateway
resource "aws_internet_gateway" "gateway" {
  vpc_id = aws_vpc.default.id
  tags = {
    Name = "${var.project_name}-${var.environment_name}-igw"
  }
}

## Internet route setup
resource "aws_route" "internet_access" {
  route_table_id         = aws_vpc.default.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.gateway.id
}

## External IP
resource "aws_eip" "gateway" {
  domain     = "vpc"
  depends_on = [aws_internet_gateway.gateway]
  tags = {
    Name = "${var.project_name}-${var.environment_name}"
  }
}

# Internal subnet
resource "aws_subnet" "private" {
  count             = 3
  cidr_block        = cidrsubnet(aws_vpc.default.cidr_block, 4, count.index)
  availability_zone = data.aws_availability_zones.available_zones.names[count.index]
  vpc_id            = aws_vpc.default.id
  tags = {
    Name = "${var.project_name}-${var.environment_name}-private-${count.index}"
  }
  depends_on = [
    aws_vpc.default,
  ]
}

# Public subnet
resource "aws_subnet" "public" {
  count                   = 3
  cidr_block              = cidrsubnet(aws_vpc.default.cidr_block, 4, count.index + 4)
  availability_zone       = data.aws_availability_zones.available_zones.names[count.index]
  vpc_id                  = aws_vpc.default.id
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.project_name}-${var.environment_name}-public-${count.index}"
  }
  depends_on = [
    aws_vpc.default,
  ]
}

# NAT gateway
resource "aws_nat_gateway" "gateway" {
  subnet_id     = aws_subnet.public[0].id
  allocation_id = aws_eip.gateway.id
  tags = {
    Name = "${var.project_name}-${var.environment_name}-nat-gw"
  }
}

# Creates a route table for private routes
resource "aws_route_table" "private" {
  count  = 3
  vpc_id = aws_vpc.default.id
  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.gateway.id
  }
  tags = {
    Name = "${var.project_name}-${var.environment_name}-rt-private"
  }
}

# Create associations between the route table and private subnets
resource "aws_route_table_association" "private" {
  count          = length(aws_subnet.private)
  subnet_id      = aws_subnet.private[count.index].id
  route_table_id = aws_route_table.private[count.index].id
}

# Database security group and rules
resource "aws_security_group" "database" {
  name   = "${var.project_name}-${var.environment_name}-database"
  vpc_id = aws_vpc.default.id
  tags = {
    Name = "${var.project_name}-${var.environment_name}-database"
  }
}
resource "aws_security_group_rule" "database_ingress_rules" {
  for_each          = { for port in var.sg_db_ports : tostring(port) => port }
  security_group_id = aws_security_group.database.id
  type              = "ingress"
  protocol          = var.sg_db_protocol
  from_port         = each.value
  to_port           = each.value
  cidr_blocks       = [var.vpc_cidr]
}
resource "aws_security_group_rule" "database_egress_rule" {
  security_group_id = aws_security_group.database.id
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = [var.vpc_cidr]
}

# External security group and rules
resource "aws_security_group" "external" {
  name   = "${var.project_name}-${var.environment_name}-external"
  vpc_id = aws_vpc.default.id
  tags = {
    Name = "${var.project_name}-${var.environment_name}-external"
  }
}
resource "aws_security_group_rule" "external_ingress_rules" {
  for_each          = { for port in var.sg_ext_ports : tostring(port) => port }
  security_group_id = aws_security_group.external.id
  type              = "ingress"
  from_port         = each.value
  to_port           = each.value
  protocol          = var.sg_ext_protocol
  cidr_blocks       = ["0.0.0.0/0"]
}
resource "aws_security_group_rule" "external_egress_rule" {
  security_group_id = aws_security_group.external.id
  type              = "egress"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

output "vpc_id" {
  value = aws_vpc.default.id
}

output "private_subnets" {
  value = aws_subnet.private[*].id
}

output "public_subnets" {
  value = aws_subnet.public[*].id
}

output "aws_security_group_ext" {
  value = aws_security_group.external.id
}

output "security_group_db" {
  value = aws_security_group.database.id
}

output "private_subnets_cidr" {
  value = aws_subnet.private[*].cidr_block
}