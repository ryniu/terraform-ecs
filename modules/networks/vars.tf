variable "environment_name" {
  type    = string
  default = ""
}

variable "project_name" {
  type    = string
  default = ""
}

variable "vpc_cidr" {
  type    = string
  default = ""
}

variable "sg_db_protocol" {
  type    = string
  default = "tcp"
}

variable "sg_ext_protocol" {
  type    = string
  default = "tcp"
}

variable "sg_db_ports" {
  type    = list(number)
  default = []
}

variable "sg_ext_ports" {
  type    = list(number)
  default = []
}