

## BASE INFRA #
module "networks" {
  source           = "./modules/networks/"
  vpc_cidr         = var.vpc_cidr
  environment_name = var.environment_name
  project_name     = var.project_name
  sg_db_ports      = [var.db_port]
  sg_ext_ports     = [var.rest-api-port, var.rest-api-gateway-port]
}

module "dns-local" {
  source         = "./modules/dns_local/"
  dns_name_local = "${var.environment_name}.${var.dns_name_local}"
  vpc_id         = module.networks.vpc_id
}

module "dns-local-ecs" {
  source         = "./modules/dns_local/"
  dns_name_local = "${var.environment_name}.${var.dns_name_local_ecs}"
  vpc_id         = module.networks.vpc_id
}

module "ecs-cluster" {
  source           = "./modules/ecs_cluster/"
  environment_name = var.environment_name
  project_name     = var.project_name
}

module "cloudwatch" {
  source           = "./modules/cloudwatch"
  environment_name = var.environment_name
  project_name     = var.project_name
  resources = {
    "rest-api"    = { resource_type = "ecs", retention_in_days = 30 },
    "restapidb" = { resource_type = "efs", retention_in_days = 30 },
  }
}

module "iam" {
  source           = "./modules/iam/"
  environment_name = var.environment_name
}


### REST-API


module "rds-rest-api" {
  source           = "./modules/rds/"
  db_name          = "restapidb"
  db_machine       = "db.t3.small"
  db_version       = "15.4"
  db_size          = "30"
  db_user          = "dbuser"
  db_pass          = "password"
  environment_name = var.environment_name
  dns_name_local   = "${var.environment_name}.${var.dns_name_local}"
  database_sg_id   = module.networks.security_group_db
  subnets          = module.networks.private_subnets
  zone_id          = module.dns-local.private_zone_id
}

module "ecs-rest-api" {
  source                           = "./modules/ecs_container"
  container_image                  = "lukaszrynski/test-rest-api:22.12.9"
  name                             = "rest-api"
  task_cpu                         = "2048"
  task_mem                         = "8192"
  container_cpu                    = "2048"
  container_mem                    = "8192"
  health_check_interval            = 90
  health_check_healthy_threshold   = 5
  enable_execute_command           = false
  health_check_unhealthy_threshold = 5
  health_check_port_app            = var.rest-api-port
  dns_name_local                   = "${var.environment_name}.${var.dns_name_local}"
  aws_region                       = var.aws_region
  project_name                     = var.project_name
  environment_name                 = var.environment_name
  cluster_id                       = module.ecs-cluster.cluster_arn
  execution_role_arn               = module.iam.ecsTaskExecutionRole
  vpc_id                           = module.networks.vpc_id
  subnets                          = module.networks.private_subnets
  security_group                   = module.networks.aws_security_group_ext
  zone_id                          = module.dns-local.private_zone_id
  ports = [
    { containerPort = var.rest-api-port, hostPort = var.rest-api-port }
  ]
  environment = [
    {
      name  = "DB_PASSWORD"
      value = module.rds-rest-api.db_pass
    },
    {
    name  = "DB_USER"
    value = module.rds-rest-api.db_user
    },
    {
      name  = "DB_HOST"
      value = module.rds-rest-api.db_dns_name
    },
    {
      name  = "DB_PORT"
      value = "5432"
    },
    {
      name  = "DB_NAME"
      value = module.rds-rest-api.db_name
    }
  ]
  depends_on = [module.rds-rest-api]
}


module "api-gateway-rest-api" {
  source  = "./modules/api-gateway"
  subnets = module.networks.private_subnets
  security_group = module.networks.aws_security_group_ext
  rest_api_listener = module.ecs-rest-api.lb_listener
  depends_on = [ module.ecs-rest-api]
}