variable "project_name" {
  type    = string
  default = "rest-api"
}
variable "account_id" {
  type    = string
  default = "063518513702"
}
variable "aws_region" {
  type    = string
  default = "eu-central-1"
}

variable "db_port" {
  type    = number
  default = "5432"
}
variable "rest-api-port" {
  type    = number
  default = "8080"
}

variable "rest-api-gateway-port" {
  type    = number
  default = "80"
}

variable "dns_name" {
  type    = string
 default = "rest-api.sii.pl"
}

variable "dns_name_local" {
  type    = string
  default = "restapi.local"
}
variable "dns_name_local_ecs" {
  type    = string
  default = "rest-api.sii.pl"
}
variable "environment_name" {
  type    = string
  default = ""
}
variable "vpc_cidr" {
  type    = string
  default = ""
}
variable "tags" {
  type    = map(string)
  default = {}
}
