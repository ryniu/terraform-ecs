environment_name       = "test"
vpc_cidr               = "10.25.1.0/24"
tags = {
  Owner       = "rest-api"
  Environment = "test"
  Terraform   = true
}